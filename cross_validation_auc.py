import config
config.Config.select_menu()
#---------------------------------------------------------------------
import numpy as np
import os
import pickle
import matplotlib.pyplot as plt
import random
from sklearn import metrics
#-----------------------------------------------------------------------------------------------------
import parse_miRTarBase as gt
import parse_TargetScan as putative
import parse_BioGRID as ppi
import parse_gene as gene

import miRNA_synthetic_data_v3 as syn
import miRNA_load_expression as data_exp
import miRNA_nmf_v5 as infer
import miRNA_draw as draw
import miRNA_score as score
import miRNA_module as module
import miRNA_score as score
import miRNA_helper as helper
#-----------------------------------------------------------------------------------------------------


if __name__ == '__main__':

    dataFile = config.Config.all_data_save
    
    miRNA, mRNA = [], []
    # if not os.path.isfile(dataFile): 
    if True:
        if config.Config.key == 'SYNTHETIC':
            nData = config.Config.num_sample
            nInput = config.Config.num_mirna
            nOutput = config.Config.num_mrna
            
            miRNA, mRNA, miRNA_list, mRNA_list, gt, gt_mi, gt_m, putative, gt_sign, ppi, corr, aux = syn.genData(\
                nData,\
                nInput,\
                nOutput,\
                config.Config.num_module,\
                config.Config.num_putative_per_mirna,\
                config.Config.signal_strength,\
                config.Config.variance_mirna,\
                config.Config.variance_mrna)
        else:

            gene_dic, _ = gene.parse_gene()
            print "loading expressions"
            d = data_exp.ExpData(gene_dic)
            miRNA_list, mRNA_list, miRNA, mRNA = d.getAllExp(mi_th=3., gene_th=10.)
            nInput = len(miRNA_list)
            nOutput = len(mRNA_list)
            nData = len(miRNA)                
            corr = d.calcCorr()

            print "loading a putative matrix"
            putative = putative.parse_prediction(miRNA_list, mRNA_list)
            
            print "loading a ppi matrix"
            ppi = ppi.get_interaction(mRNA_list)
        

            print "loading ground-truth pairs"
            gt, gt_mi, gt_m = gt.parse_groundTruth(miRNA_list, mRNA_list) 

            putative_replace_mirtarbase = gt 
            
            aux = [[],[],[]]
            gt_sign = score.convert_corr2sign(corr, gt, 0.001)
            
        if config.Config.save_enable == "Enable":
            with open(dataFile, 'wb') as fdat:
                pickle.dump(
                    (
                        miRNA, mRNA, nInput, nOutput, miRNA_list, mRNA_list,
                        nData, gt, gt_mi, gt_m, putative, corr, ppi, aux, gt_sign
                    ),
                    fdat
                )  
    else:
        miRNA, mRNA, nInput, nOutput, miRNA_list, mRNA_list, nData, gt, gt_mi, gt_m, putative, corr, ppi, aux, gt_sign = pickle.load( open( dataFile, "rb" ) )


    miRNA, mRNA, mRNA_mean, mRNA_var = helper.normalize(miRNA, mRNA, nInput, nOutput)        
    
    
    print nInput, nOutput
    print "miRNA: ", np.shape(miRNA)
    print "mRNA: ", np.shape(mRNA)
    print "corr: ", np.shape(corr)
    print "putative_replace_mirtarbase: ", np.shape(putative_replace_mirtarbase)                    
    print "gt: ", np.shape(gt)            
    print "ppi: ", np.shape(ppi)
    corr = np.abs(corr)
    print np.max(corr), np.min(corr), np.mean(corr)

    # ----------------------------------------------------------------------------------------------

    # Start the cross validation
    auc = []
    random_list = range(len(miRNA_list))
    random.shuffle(random_list)
    for i in range(10):
        putative_replace_mirtarbase_temp = putative_replace_mirtarbase
        for j in random_list[(int(len(miRNA_list)/10))*i : (int(len(miRNA_list)/10))*(i+1)]:
            for k in xrange(len(mRNA_list)):
                putative_replace_mirtarbase_temp[j][k] = 0

        params = {
            'summary_active': False,
            'learning_rate': 0.01,
            'M': nInput,
            'N': nOutput,
            'K': config.Config.num_module,
            'num_v': nOutput,
            'num_u': nInput,
            'T': 0.95,
            'I_Phi': putative_replace_mirtarbase_temp,
            'I_Omega': ppi,
            'X': miRNA,
            'Y': mRNA,
            'y_var': mRNA_var,
            'y_mean': mRNA_mean,
            'n_sample': nData,
            'n_batch': 256,        
            'n_epoch': 1000,
            'miRNA_names': miRNA_list,
            'mRNA_names': mRNA_list,
            'gt': [gt_mi, gt_m, putative, corr, gt],
            'U_initial': None,
            'V_initial': None  
        }    

        S,W,U,V = infer.train(params)
        thresh = [0.001,0.005,0.01,0.03,0.05,0.06,0.07,0.08,0.09,0.1,0.15,0.2]
        tpr = []
        fpr = []
        for th in thresh:
            p, r, f, a, tp, fp, tn, fn = score.calcScore2(S*W*putative, gt_sign, th, putative_replace_mirtarbase_temp)
            tpr.append(float(float(tp)/(float(tp)+float(fn))))
            fpr.append(float(float(tn)/(float(tn)+float(fp))))

        auc.append(np.trapz(y=tpr,x=fpr))
    
    print "The mean AUC over ten folds is"
    print np.mean(auc)
        
    