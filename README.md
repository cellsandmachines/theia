# README #

## List of main files ##

* config.py: select the type of data

* config.ini: Specify the names of input files and save files for each data type

* parse_miRTarBase.py: parse ground truth pairs

* parse_TargetScan.py: parse sequence-based prediction results

* parse_BioGRID.py: parse protein-protein interactions

* parse_gene.py: parse genes that are related with a particular disease

* miRNA_load_expression: parse expression data

* miRNA_main.py: main file that handles Bayesian ooptimization loop

* miRNA_nmf_v5.py: core implementation of a leanring algorithm




## Config.ini ##

Data type BRCA starts with [BCRA]. In the future, if you want add another data, copy and paste the following bur with a different [XXX].
The following variables should be defined.



* thCnt: The number of papers that have confirmed that a pair of an miRNA and an mRNA is interacting.
* gene_source: all_gene_disease_associations.tsv from http://www.disgenet.org/web/DisGeNET/menu
* gene_key: name of disease (e.g., breast)
* gt_source: Ground truth text file downloaded from http://mirtarbase.mbc.nctu.edu.tw/php/search.php#disease
* putative_source: Sequence based prediction results from http://www.targetscan.org/cgi-bin/targetscan/data_download.vert71.cgi (Specifically, Conserved_Site_Context_Scores.txt and Nonconserved_Site_Context_Scores.txt)
* putative_save: Parsed data for the sequence based prediction results
* ppi_source: BIOGRID-ALL-3.4.155.tab2.txt from https://thebiogrid.org/
* ppi_save: Parsed data for ppi's
* mirna_exp_source: miRNA expression raw data (from TCGA)
* mrna_exp_source: mRNA expression raw data (from TCGA)
* mirna_save: parsed miRNA expression data
* mrna_save: parsed mRNA expression data
* selected_exp_save: A text-form file of miRNA/mRNA expressions, plus Pearson correlation coefficients
* all_data_save: Final-stage parsed data
* model_save: Network model save file prefix
* results: prefix of the results folder (e.g., BRCA_results_)
* save_enable: Enable or Disable (to choose to save training results)


## How to execute ##

* $ python miRNA_main.py

## Running cross validation ##

* $ python cross_validation_auc.py
