import numpy as np


#-----------------------------------------------------------------------------------------------------
def getStat(mRNA, mRNA_num):
    mean = np.zeros(mRNA_num)
    variance = np.zeros(mRNA_num)
    for i in xrange(mRNA_num):    
        mean[i] = np.mean(mRNA[:,i])
        variance[i] = np.var(mRNA[:,i])
    return mean, variance
#-----------------------------------------------------------------------------------------------------
def normalize(miRNA, mRNA, nInput, nOutput):
    mean, var = getStat(miRNA, nInput)
    # print mean
    # print var
    miRNA = np.divide(miRNA-mean, np.sqrt(var)) + 3*np.ones((1,1))
    mean, var = getStat(miRNA, nInput)
    # print mean
    # print var    
    mRNA_mean, mRNA_var = getStat(mRNA, nOutput)
    # print mRNA_mean
    # print mRNA_var    
    mRNA = np.divide(mRNA, mRNA_mean) + 10*np.ones((1,1))
    mRNA_mean, mRNA_var = getStat(mRNA, nOutput)
    # print mRNA_mean
    # print mRNA_var
    
    return miRNA, mRNA, mRNA_mean, mRNA_var
#--------------