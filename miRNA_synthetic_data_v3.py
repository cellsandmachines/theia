import numpy as np
import random
import scipy.stats

from itertools import chain, groupby, islice, repeat, tee
from matplotlib import pyplot 
from networkx import from_numpy_matrix, draw_networkx
from numpy import arange, array, clip, cumsum, fromiter, insert, matmul, sqrt, zeros
from numpy.random import normal, randint, randn, shuffle
from scipy.stats import pearsonr



def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def flatmap(func, *iterable):
    return chain(*map(func, *iterable))


def randn_skew(n, alpha=0.0, loc=0.0, scale=1.0):
    sigma = alpha / sqrt(1.0 + alpha ** 2) 
    u0 = randn(n)
    u1 = (sigma * u0 + sqrt(1.0 - sigma ** 2) * randn(n)) * scale
    u1[u0 < 0] *= -1
    return u1 + loc


def generate_modules(max_items, max_modules):
    ns = randn_skew(max_items, alpha=5, loc=1, scale=1).astype(int)
    ns[ns < 0] = 0
    ps = fromiter(flatmap(repeat, *zip(*enumerate(ns))), dtype=int)
    shuffle(ps)
    sizes = randn_skew(max_modules, alpha=5, loc=5, scale=5).astype(int)
    indices = insert(cumsum(sizes), 0, 0)
    ret = list(ps[b:e].tolist() for b, e in pairwise(indices) if b < len(ps))
    return ret + [] * (max_modules - len(ret))


def matrix_from_modules(modules, N):
    M = zeros((N, len(modules)))
    for i, m in enumerate(array(m) for m in modules):
        M[m, i] = 1
    return M 


#-----------------------------------------------------------------------------------------------------
def getStat(miRNA):
    mean = np.mean(miRNA, axis=0)
    variance = np.var(miRNA, axis=0)
    return mean, variance
#-----------------------------------------------------------------------------------------------------       

a, b = -1, 5       
#-----------------------------------------------------------------------------------------------------    
def genData(nSample,\
            M,\
            N,\
            K,\
            num_putative_per_mirna,\
            signal_strength,\
            variance_mirna,\
            variance_mrna):
            
            
    """
        Sythetic data generator

        Arg:
            signal_strength:    regulation weight (or strength of regulation)
            variance_mirna:         variance of an miRNA expression
            variance_mrna:          variance of an mRNA expression
        Return:
            x_list:     list of miRNA vectors, each of which is a numpy array of dim(1,M)
            y_list:     list of mRNA vectors, each of which is a numpy array of dim(1,N)
    """
    
    x_list = []
    y_list = []
    gt_mi = []
    gt_m = []
    x_name = [str(i) for i in range(M)]
    y_name = [str(i) for i in range(N)]
    
    # U = np.zeros((M, K))
    # V = np.zeros((N, K))
    true_module_mi = []
    true_module_m = []    
    # print np.shape(U), np.shape(V)
    
    modules_m, modules_n = generate_modules(M, K), generate_modules(N, K)
    print len(modules_m), len(modules_n)
    true_module = list(zip(modules_m, modules_n))
    m = min(len(modules_m), len(modules_n))
    U, V = matrix_from_modules(modules_m[:m], M), matrix_from_modules(modules_n[:m], N)	
    

    
    
    putative = (matmul(U, V.transpose()) > 0) * 1
    M, N = putative.shape
    ppi = (matmul(V, V.transpose()) > 0) * 1
 
    #module = range(K)
    #for i in xrange(M):          
    #    b = num_items_per_module
    #    r = scipy.stats.truncnorm.rvs(a, b, loc=1, scale=1, size=1)
    #    r = int(np.round(r))
        
    #    select_module = random.sample(module, r)
    #    U[i,select_module] = 1
    #    true_module_mi.append(select_module)
    #for i in xrange(N):
    #    b = num_items_per_module
    #    r = scipy.stats.truncnorm.rvs(a, b, loc=1, scale=1, size=1)
    #    r = int(np.round(r))
        
    #    select_module = random.sample(module, r)
    #    V[i,select_module] = 1
    #    true_module_m.append(select_module)
    #true_module = zip(true_module_mi, true_module_m)

    ground_truth = (np.matmul(U, np.transpose(V))>0)*1
    ppi = (np.matmul(V, np.transpose(V))>0)*1
    for i in range(N):
        ppi[i,i] = 1

    prediction = np.copy(ground_truth)
    for i in range(M):    
        mask = np.where(ground_truth[i,:]==0)[0]
        num = num_putative_per_mirna - (N-len(mask))
        
        if num > 0:
            pred = random.sample(list(mask), num)
            prediction[i, pred] = 1
            
            # print i, mask, num, pred
        
    ground_truth_sign = -1*np.copy(ground_truth)
    for i in range(M):    
        mask = np.where(ground_truth[i,:]>0)[0]
        sign = random.sample(list(mask), int(len(mask)*0.2) )
        ground_truth_sign[i, sign] = 1  
    
    for i in range(M):
        for j in range(N):
            if ground_truth[i,j]>0:
                gt_mi.append(i)
                gt_m.append(j)

                
    for s in range(nSample):
        x = np.zeros((1, M))
        for i in range(M):
            x[0,i] = np.random.normal(0, variance_mirna)
        x_list.append(x)
        
    x_list = np.vstack(x_list)        
    # print np.shape(x_list)
    # mean, var = getStat(x_list)
    # print "mean, var: ", mean, var
    x_list_s = np.divide(x_list, np.sqrt(variance_mirna)) + 3*np.ones((1,1)) # N(3, 1)
    
    
    for s in range(nSample):        
        y = np.zeros((1, N))
        for j in range(N):
            gt = ground_truth[:, j]
            idx = [i for i in range(M) if gt[i] == 1.0]
            regulation = 0.0

            for g in idx:
                regulation += ground_truth_sign[g,j] * signal_strength * x_list_s[s,g]
            y[0,j] = np.random.normal(10 + regulation, np.sqrt(variance_mrna)) # N(10+reg, 1)
        
        y_list.append(y)    
        
    y_list = np.vstack(y_list)
    corr = np.zeros((M, N))
    for i in range(M):    
        sep = ','
        for j in range(N):    
            r, p = scipy.stats.pearsonr(x_list[:,i], y_list[:,j])
            corr[i, j] = r

    return x_list, y_list, x_name, y_name, ground_truth, gt_mi, gt_m, prediction, ground_truth_sign, ppi, corr, [U,V, true_module]
