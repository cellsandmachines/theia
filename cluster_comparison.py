from __future__ import division
from __future__ import print_function
from matplotlib.pyplot import figure, legend, plot, show, xlabel, ylabel, savefig
from numpy import fromiter, linspace


def j(a, b):
    return len(a & b) / len(a | b)


def similarity(our_clusters, truth_clusters, x):
    return (sum(1 for a in our_clusters if max(j(a, b) for b in truth_clusters) > x)) / len(our_clusters)


def discovery(our_clusters, truth_clusters, x):
    discovered = [False] * len(truth_clusters)
    for cluster in our_clusters:
        i, similarity = max(enumerate(j(cluster, b) for b in truth_clusters), key=lambda x: x[1])
        if similarity > x:
            discovered[i] = True
    return sum(discovered) / len(discovered)


def analyze(our_clusters, their_clusters, truth_clusters):
    our_overall = sum(max(j(a, b) for b in truth_clusters) for a in our_clusters) / len(our_clusters)
    their_overall = sum(max(j(a, b) for b in truth_clusters) for a in their_clusters) / len(our_clusters)

    print('Our overall =', our_overall)
    print('Their overall =', their_overall)

    xs = linspace(0, 1, 1000)
    ys = fromiter((similarity(our_clusters, truth_clusters, x) for x in xs), dtype='float')
    ys2 = fromiter((similarity(their_clusters, truth_clusters, x) for x in xs), dtype='float')
    figure(facecolor='white')
    xlabel('Min. Similarity')
    plot(xs, ys, label='Theia')
    ylabel('Ratio of Clusters')
    plot(xs, ys2, label='SNMNMF')
    legend()
    show()
    # savefig('cluster_ratio.pdf')

    ys = fromiter((discovery(our_clusters, truth_clusters, x) for x in xs), dtype='float')
    ys2 = fromiter((discovery(their_clusters, truth_clusters, x) for x in xs), dtype='float')
    figure(facecolor='white')
    xlabel('Min. Similarity')
    plot(xs, ys, label='Theia')
    ylabel('Discovery Rate')
    plot(xs, ys2, label='SNMNMF')
    legend()
    show()
    # savefig('cluster_discovery.pdf')
def analyze2(our_clusters, truth_clusters):
    our_overall = sum(max(j(a, b) for b in truth_clusters) for a in our_clusters) / len(our_clusters)

    print('Our overall =', our_overall)

    xs = linspace(0, 1, 1000)
    ys = fromiter((similarity(our_clusters, truth_clusters, x) for x in xs), dtype='float')
    figure(facecolor='white')
    xlabel('Min. Similarity')
    plot(xs, ys, label='Theia')
    legend()
    show()

    ys = fromiter((discovery(our_clusters, truth_clusters, x) for x in xs), dtype='float')
    figure(facecolor='white')
    xlabel('Min. Similarity')
    plot(xs, ys, label='Theia')
    legend()
    show()    

def example():

    # This just generates some fake clusters for my testing. You can get rid of this...
    from random import choice, randint, sample

    possible_choices = ''.join(chr(x) for x in range(32, 127))

    def modify(s, p):
        t = set(s)
        for x in sample(t, randint(0, int(p * len(t)))):
            t.remove(x)
        for x in (choice(possible_choices) for _ in range(randint(0, int(p * len(t))))):
            t.add(x)
        return t

    truth_clusters = [set(choice(possible_choices) for _ in range(randint(2, 7))) for _ in range(20)]
    our_clusters = [modify(c, 0.33) for c in truth_clusters]
    our_clusters = [c for c in our_clusters if len(c) > 0]
    their_clusters = [modify(c, 1) for c in truth_clusters]
    their_clusters = [c for c in their_clusters if len(c) > 0]

    print(truth_clusters)
    # XXX: Substitue your clusters here
    analyze(our_clusters, their_clusters, truth_clusters)
    
    
