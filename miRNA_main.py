import config
config.Config.select_menu()
#---------------------------------------------------------------------
import numpy as np
import os
import pickle
import matplotlib.pyplot as plt
#-----------------------------------------------------------------------------------------------------
import parse_miRTarBase as gt
import parse_TargetScan as putative
import parse_BioGRID as ppi
import parse_gene as gene

import miRNA_synthetic_data_v3 as syn
import miRNA_load_expression as data_exp
import miRNA_nmf_v5 as infer
import miRNA_draw as draw
import miRNA_score as score
import miRNA_module as module
import miRNA_score as score
import miRNA_helper as helper
#-----------------------------------------------------------------------------------------------------


if __name__ == '__main__':

    dataFile = config.Config.all_data_save
    
    miRNA, mRNA = [], []
    if True:
        if config.Config.key == 'SYNTHETIC':
            nData = config.Config.num_sample
            nInput = config.Config.num_mirna
            nOutput = config.Config.num_mrna
            
            miRNA, mRNA, miRNA_list, mRNA_list, gt, gt_mi, gt_m, putative, gt_sign, ppi, corr, aux = syn.genData(\
                nData,\
                nInput,\
                nOutput,\
                config.Config.num_module,\
                config.Config.num_putative_per_mirna,\
                config.Config.signal_strength,\
                config.Config.variance_mirna,\
                config.Config.variance_mrna)
        else:

            gene_dic, _ = gene.parse_gene()
            print "loading expressions"
            d = data_exp.ExpData(gene_dic)
            miRNA_list, mRNA_list, miRNA, mRNA = d.getAllExp(mi_th=3., gene_th=10.)
            nInput = len(miRNA_list)
            nOutput = len(mRNA_list)
            nData = len(miRNA)                
            corr = d.calcCorr()
            
            print "loading a putative matrix"
            putative = putative.parse_prediction(miRNA_list, mRNA_list)
            
            print "loading a ppi matrix"
            ppi = ppi.get_interaction(mRNA_list)        

            print "loading ground-truth pairs"
            gt, gt_mi, gt_m = gt.parse_groundTruth(miRNA_list, mRNA_list)              
            # correct false negatives in putative interactions from experimentally-validated results
            putative = np.logical_or(putative,gt)*1
            
            aux = [[],[],[]]
            gt_sign = score.convert_corr2sign(corr, gt, 0.001)
            
        if config.Config.save_enable == "Enable":
            with open(dataFile, 'wb') as fdat:
                pickle.dump(
                    (
                        miRNA, mRNA, nInput, nOutput, miRNA_list, mRNA_list,
                        nData, gt, gt_mi, gt_m, putative, corr, ppi, aux, gt_sign
                    ),
                    fdat
                )  
    else:
        miRNA, mRNA, nInput, nOutput, miRNA_list, mRNA_list, nData, gt, gt_mi, gt_m, putative, corr, ppi, aux, gt_sign = pickle.load( open( dataFile, "rb" ) )


    miRNA, mRNA, mRNA_mean, mRNA_var = helper.normalize(miRNA, mRNA, nInput, nOutput)        
        
    print nInput, nOutput
    print "miRNA: ", np.shape(miRNA)
    print "mRNA: ", np.shape(mRNA)
    print "corr: ", np.shape(corr)
    print "putative: ", np.shape(putative)                    
    print "gt: ", np.shape(gt)            
    print "ppi: ", np.shape(ppi)
    corr = np.abs(corr)
    print np.max(corr), np.min(corr), np.mean(corr)
   
    params = {
        'summary_active': False,
        'learning_rate': 0.01,
        'M': nInput,
        'N': nOutput,
        'K': config.Config.num_module,
        'num_v': nOutput,
        'num_u': nInput,
        'T': 0.95,
        'I_Phi': putative,
        'I_Omega': ppi,
        'X': miRNA,
        'Y': mRNA,
        'y_var': mRNA_var,
        'y_mean': mRNA_mean,
        'n_sample': nData,
        'n_batch': 256,        
        'n_epoch': 1000,
        'miRNA_names': miRNA_list,
        'mRNA_names': mRNA_list,
        'gt': [gt_mi, gt_m, putative, corr, gt],
        'U_initial': None,
        'V_initial': None
        # 'U_initial': aux[0],
        # 'V_initial': aux[1]       
    }    

    S,W,U,V = infer.train(params)
    modules = module.find_modules(miRNA_list, mRNA_list, U, V, th=0.3)    
    mi = modules[0]
    m = modules[1]
    
    print modules
        
    th = 0.05
    p, r, f, a, tp, fp, tn, fn = score.calcScore2(S*W*putative, gt_sign, th, putative)
    print p, r, f, a, tp, fp, tn, fn
       
    draw.plot_heatmap(params, S*W*putative,U,V, S=S, corr=corr, gt=gt, putative=putative)        